# SARS-CoV-2 Analysis 
# By-Aarush Kumar
Dated:May 09,2021
Analysing SARS_CoV-2 from government provided datasets.

Steps involved:
1. Importing required packages.
2. Gathering Data.
3. Data Wrangling.
*Data Wrangling is a process where we will transform and clean our data to our needs. We can’t analyze with our raw  extracted data. So, we have to transform the data to proceed with our analysis.*
4. Exploratory Data Analysis (EDA) and Visualization.

**Basically this project mainly aims for Covid cases only for India as I didn't have accurate datasets for world and that's why I used previous datasets.**

Datasets can be assessed from:
https://drive.google.com/drive/folders/1sIlViGRa5pRaq3HsL2GBnbHU6gjRzHpy?usp=sharing

## Steps followed are as:
### Data gathering and uploading:
![Screenshot_from_2021-05-25_06-15-39](/uploads/bdfdbd9a00f5a2e1dae3b80c7417a07b/Screenshot_from_2021-05-25_06-15-39.png)
### Data Wrangling:
![Screenshot_from_2021-05-25_06-15-59](/uploads/2090278360adb5340fbefec2686fabc0/Screenshot_from_2021-05-25_06-15-59.png)
### Data Visualization:
![Screenshot_from_2021-05-25_06-16-30](/uploads/a80ec71bfd01e8952f317f294c87af48/Screenshot_from_2021-05-25_06-16-30.png)
![Screenshot_from_2021-05-25_06-16-49](/uploads/33900018b97cae9f5c1b7de4031b7fb3/Screenshot_from_2021-05-25_06-16-49.png)
### EDA:
![Screenshot_from_2021-05-25_06-17-06](/uploads/47fc70ea669bcdb6b803cd79cb48c149/Screenshot_from_2021-05-25_06-17-06.png)
![Screenshot_from_2021-05-25_06-18-17](/uploads/5bc7f7ed14433c522de35ec4eb85fd76/Screenshot_from_2021-05-25_06-18-17.png)

## Thankyou!
